'use strict';

describe('Controller: PraticeCtrl', function () {

  // load the controller's module
  beforeEach(module('mathMavensApp'));

  var PraticeCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    PraticeCtrl = $controller('PraticeCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(PraticeCtrl.awesomeThings.length).toBe(3);
  });
});
