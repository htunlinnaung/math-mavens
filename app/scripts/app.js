'use strict';

/**
 * @ngdoc overview
 * @name mathMavensApp
 * @description
 * # mathMavensApp
 *
 * Main module of the application.
 */

 /*
'ngRoute',
  	'ngAnimate',
    'ui.bootstrap',
    'ngQuantum',
    'angular-tour',
    'as.sortable',
    'ngDragDrop',
    'ngDialog',
    'validation.match'
 */
angular
  .module('mathMavensApp', [
  	'ngRoute',
  	'ngAnimate',
    'ui.bootstrap',
    'ngDragDrop',
    'ngDialog'

  ])
  .config(['$routeProvider',
  function($routeProvider) {
    $routeProvider
      .when('/Book/:bookId', {
        templateUrl: 'views/book.html',
        controller: 'BookCtrl',
        controllerAs: 'book'
      })
      .when('/Book/:bookId/ch/:chapterId', {
        templateUrl: 'views/chapter.html',
        controller: 'ChapterCtrl',
        controllerAs: 'chapter'
      })
      /*.when('/example-1', {
        templateUrl: 'views/chapter.html',
        controller: 'ChapterCtrl',
        controllerAs: 'chapter'
      })
      .when('/example-2', {
        templateUrl: 'views/chapter.html',
        controller: 'ChapterCtrl',
        controllerAs: 'chapter'
      })*/
      .when('/step-1', {
        templateUrl: 'views/step-1.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      }).when('/step-2', {
        templateUrl: 'views/step-2.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      }).when('/step-3', {
        templateUrl: 'views/step-3.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      }).when('/step-4', {
        templateUrl: 'views/step-4.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      }).when('/step-5', {
        templateUrl: 'views/step-5.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      }).when('/uitest', {
        templateUrl: 'views/ui-test.html',
        controller: 'DragCtrl',
        controllerAs: 'loki'
      }).when('/pratice', {
        templateUrl: 'views/pratice.html',
        controller: 'PraticeCtrl',
        controllerAs: 'pratice'
      }).when('/learn', {
        templateUrl: 'views/learn.html',
        controller: 'LearnCtrl',
        controllerAs: 'learn'
      });

    // $locationProvider.html5Mode(true);
}]);