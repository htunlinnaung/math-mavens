'use strict';

/**
 * @ngdoc function
 * @name mathMavensApp.controller:PraticeCtrl
 * @description
 * # PraticeCtrl
 * Controller of the mathMavensApp
 */
angular.module('mathMavensApp')
    .controller('PraticeCtrl', function($scope, ngDialog, ngDragDropService) {
        $scope.list1 = [];
        $scope.list2 = [];
        $scope.list3 = [];
        $scope.list4 = [];
        $scope.example1 = {
            "title": "Example 1",
            "question": "Jonathan had 3/4 as much money as Kelvin at first.After each of them spent 2/5 of their money, Kelvin had $87 more than Jonathan. How much money did Jonathan spend?",
            "inputinfo": {
                "one": "Jonathan had 3/4 as much money as Kelvin at first.",
                "two": "Each of them spent 2/5 of their money,therefore left with 3/5.",
                "three": "Kelvin had $87 more than Jonathan."
            },
            "variables": {
                "one": "J",
                "two": "K"
            },
            "inputbefore": {
                "one": "3",
                "two": "4"
            },
            "adjustbefore": {
                "one": "15",
                "two": "20"
            },
            "change": {
                "one": "3/5",
                "two": "3/5"
            },
            "after": {
                "one": "9",
                "two": "12"
            },
            "final": "87"

        };

        $scope.example2 = {
            "title": "Example 2",
            "question": "Gwen and Henry shared some sweets in the ratio of 9:5. Gwen gave away 5/6 of her sweets while Henry bought some more sweets to double the number that he initially had. In the end,Henry had 34 more sweets than Gwen. How many sweets did Gwen have at first?",
            "inputinfo": {
                "one": "Sweets shared by Gwen and Henry in ratio of 9:5",
                "two": "Gwen gave away 5/6 of her sweets whereas Henry bought some more sweets to double his initail number.",
                "three": "Henry had 34 more sweets than Gwen in the end"
            },
            "variables": {
                "one": "G",
                "two": "H"
            },
            "inputbefore": {
                "one": "9",
                "two": "5"
            },
            "adjustbefore": {
                "one": "18",
                "two": "10"
            },
            "change": {
                "one": "1/6",
                "two": "2"
            },
            "after": {
                "one": "3",
                "two": "20"
            },
            "final": "34"

        };

        $scope.list5 = [{
            'title': 'Item 1',
            'drag': true
        }, {
            'title': 'Item 2',
            'drag': true
        }, {
            'title': 'Item 3',
            'drag': true
        }, {
            'title': 'Item 4',
            'drag': true
        }, {
            'title': 'Item 5',
            'drag': true
        }, {
            'title': 'Item 6',
            'drag': true
        }, {
            'title': 'Item 7',
            'drag': true
        }, {
            'title': 'Item 8',
            'drag': true
        }, {
            'title': 'Item 9',
            'drag': true
        }, {
            'title': 'Item 10',
            'drag': true
        }];

        $scope.realvalues = {
            'list1': 'Item 1',
            'list2': 'Item 3',
            'list3': 'Item 3',
            'list4': 'Item 4',

        };
        $scope.onDrop = function() {

            if (ngDragDropService.draggableScope.item.title === $scope.realvalues.list1) {
                ngDialog.open({
                    template: 'Correct',
                    plain: true
                });
            } else {
                ngDialog.open({
                    template: 'That is not correct',
                    plain: true
                });
            }
        };

        // Limit items to be dropped in list1
        $scope.optionsList1 = {
            accept: function() {
                if ($scope.list1.length >= 1) {
                    return false;
                } else {
                    return true;
                }

            }
        };

        $scope.optionsList2 = {
            accept: function() {
                if ($scope.list2.length >= 1) {
                    return false;
                } else {
                    return true;
                }
            }
        }
    });