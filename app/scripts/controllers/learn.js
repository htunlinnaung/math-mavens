'use strict';

/**
 * @ngdoc function
 * @name mathMavensApp.controller:LearnCtrl
 * @description
 * # LearnCtrl
 * Controller of the mathMavensApp
 */
angular.module('mathMavensApp')
  .controller('LearnCtrl', function($scope) {
    $scope.example1 = {
			"title":"Example 1",
			"question":"Jonathan had 3/4 as much money as Kelvin at first.After each of them spent 2/5 of their money, Kelvin had $87 more than Jonathan. How much money did Jonathan spend?",
			"inputinfo":{
				"one":"Jonathan had 3/4 as much money as Kelvin at first.",
				"two":"Each of them spent 2/5 of their money,therefore left with 3/5.",
				"three":"Kelvin had $87 more than Jonathan."
			},
			"variables":{
				"one":"J",
				"two":"K"
			},
			"inputbefore":{
				"one":"3",
				"two":"4"
			},
			"adjustbefore":{
				"one":"15",
				"two":"20"
			},
			"change":{
				"one":"3/5",
				"two":"3/5"
			},
			"after":{
				"one":"9",
				"two":"12"
			},
			"final":"87"
				
		};

	$scope.example2 = {
		"title":"Example 2",
		"question":"Gwen and Henry shared some sweets in the ratio of 9:5. Gwen gave away 5/6 of her sweets while Henry bought some more sweets to double the number that he initially had. In the end,Henry had 34 more sweets than Gwen. How many sweets did Gwen have at first?",
		"inputinfo":{
			"one":"Sweets shared by Gwen and Henry in ratio of 9:5",
			"two":"Gwen gave away 5/6 of her sweets whereas Henry bought some more sweets to double his initail number.",
			"three":"Henry had 34 more sweets than Gwen in the end"
		},
		"variables":{
			"one":"G",
			"two":"H"
		},
		"inputbefore":{
			"one":"9",
			"two":"5"
		},
		"adjustbefore":{
			"one":"18",
			"two":"10"
		},
		"change":{
			"one":"1/6",
			"two":"2"
		},
		"after":{
			"one":"3",
			"two":"20"
		},
		"final":"34"
			
	};
		
    	
  });
