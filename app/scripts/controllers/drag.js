'use strict';

angular.module('mathMavensApp')
    .controller('DragCtrl', function($rootScope, $scope, $q, ngDialog, ngDragDropService) {
        $scope.list1 = [];
        $scope.list2 = [];
        $scope.list3 = [];
        $scope.list4 = [];
        $scope.list5 = [{
            'title': 'Item 1',
            'drag': true
        }, {
            'title': 'Item 2',
            'drag': true
        }, {
            'title': 'Item 3',
            'drag': true
        }, {
            'title': 'Item 4',
            'drag': true
        }, {
            'title': 'Item 5',
            'drag': true
        }, {
            'title': 'Item 6',
            'drag': true
        }, {
            'title': 'Item 7',
            'drag': true
        }, {
            'title': 'Item 8',
            'drag': true
        }, {
            'title': 'Item 9',
            'drag': true
        }, {
            'title': 'Item 10',
            'drag': true
        }];

        /*$scope.list1 = [
        	{'title': 'U','drag':true},
        	{'title': 'O','drag':true},
        	{'title': 'M','drag':true},
        	{'title': 'L','drag':true},
        	{'title': 'G','drag':true},
        	{'title': 'L','drag':true},

        ];

        this.dropCallback = function(event,ui,title,$index){
        	if($scope.list1.map(function(item){
        			return item.title;
	        	}).join('') === 'GOLLUM'){
	        		$scope.list1.forEach(function(value,key){
	        			$scope.list1[key].drag = false;
	        	});
        	}
        };*/

        $scope.realvalues = {
            'list1': 'Item 1',
            'list2': 'Item 3',
            'list3': 'Item 3',
            'list4': 'Item 4',

        };

        /*this.dropCallback = function(event,ui,title,$index){
        	if($scope.list1.title)
        }*/

        $scope.onDrop = function() {

            if (ngDragDropService.draggableScope.item.title === $scope.realvalues.list1) {
                ngDialog.open({
                    template: 'Correct',
                    plain: true
                });
            } else {
                ngDialog.open({
                    template: 'That is not correct',
                    plain: true
                });
            }
        };

        // Limit items to be dropped in list1
        $scope.optionsList1 = {
            accept: function() {
                if ($scope.list1.length >= 1) {
                    return false;
                } else {
                    return true;
                }

            }
        };

        $scope.optionsList2 = {
            accept: function() {
                if ($scope.list2.length >= 1) {
                    return false;
                } else {
                    return true;
                }
            }
        };
        $scope.optionsList3 = {
            accept: function() {
                if ($scope.list3.length >= 1) {
                    return false;
                } else {
                    return true;
                }
            }
        };
        $scope.optionsList4 = {
            accept: function() {
                if ($scope.list4.length >= 1) {
                    return false;
                } else {
                    return true;
                }
            }
        };

        $scope.popup1 = function() {
            ngDialog.open({
                template: 'views/popup-1.html'
            });
        };
        $scope.popup2 = function() {
            ngDialog.open({
                template: 'views/popup-2.html'
            });
        };
        /*$scope.popup3 = ngDialog.open({
		    template: 'views/popup-3.html'
		});
	$scope.popup4 = ngDialog.open({
		    template: 'views/popup-4.html'
		});  
	$scope.popup5 = ngDialog.open({
		    template: 'views/popup-5.html'
		}); */

        /*$scope.beforeDrop = function() {
    var deferred = $q.defer();
    if (confirm('Are you sure???')) {
      deferred.resolve();
    } else {
      deferred.reject();
    }
    return deferred.promise;
  };*/

        /*if(ngDragDropService.draggableScope.item.title == $scope.realvalues.list1){
      	return true;
      }else{
      	return false;
      }*/
    });