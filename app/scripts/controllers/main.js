'use strict';

/**
 * @ngdoc function
 * @name mathMavensApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the mathMavensApp
 */
angular.module('mathMavensApp')
    .config(['$httpProvider',
        function($httpProvider) {
            /*delete $httpProvider.defaults.headers.common['X-Requested-With'];
	    $httpProvider.defaults.headers.post['Accept'] = 'application/json, text/javascript';
	    $httpProvider.defaults.headers.post['Content-Type'] = 'application/json; charset=utf-8';
	    $httpProvider.defaults.headers.post['Access-Control-Max-Age'] = '1728000';
	    $httpProvider.defaults.headers.common['Access-Control-Max-Age'] = '1728000';
	    $httpProvider.defaults.headers.common['Accept'] = 'application/json, text/html';
	    $httpProvider.defaults.headers.common['Content-Type'] = 'text/html; charset=utf-8';
    	$httpProvider.defaults.useXDomain = true;*/
            $httpProvider.defaults.headers.get = {
                'Accept': 'application/json, text/javascript, text/html',
                'Content-Type': 'application/json; charset=utf-8'
            };
        }
    ])
    .controller('MainCtrl', function($scope, $http) {

    	$scope.example1 = {
			"title":"Example 1",
			"question":"Jonathan had 3/4 as much money as Kelvin at first.After each of them spent 2/5 of their money, Kelvin had $87 more than Jonathan. How much money did Jonathan spend?",
			"inputinfo":{
				"one":"Jonathan had 3/4 as much money as Kelvin at first.",
				"two":"Each of them spent 2/5 of their money,therefore left with 3/5.",
				"three":"Kelvin had $87 more than Jonathan."
			},
			"variables":{
				"one":"J",
				"two":"K"
			},
			"inputbefore":{
				"one":"3",
				"two":"4"
			},
			"adjustbefore":{
				"one":"15",
				"two":"20"
			},
			"change":{
				"one":"3/5",
				"two":"3/5"
			},
			"after":{
				"one":"9",
				"two":"12"
			},
			"final":"87"
				
		};

		$scope.example2 = {
			"title":"Example 2",
			"question":"Gwen and Henry shared some sweets in the ratio of 9:5. Gwen gave away 5/6 of her sweets while Henry bought some more sweets to double the number that he initially had. In the end,Henry had 34 more sweets than Gwen. How many sweets did Gwen have at first?",
			"inputinfo":{
				"one":"Sweets shared by Gwen and Henry in ratio of 9:5",
				"two":"Gwen gave away 5/6 of her sweets whereas Henry bought some more sweets to double his initail number.",
				"three":"Henry had 34 more sweets than Gwen in the end"
			},
			"variables":{
				"one":"G",
				"two":"H"
			},
			"inputbefore":{
				"one":"9",
				"two":"5"
			},
			"adjustbefore":{
				"one":"18",
				"two":"10"
			},
			"change":{
				"one":"1/6",
				"two":"2"
			},
			"after":{
				"one":"3",
				"two":"20"
			},
			"final":"34"
				
		};
		
    	$scope.highlight = function(ele){
    		//console.log('here');
    		angular.element('span').addClass('highlight');
    		$scope.step1 = false;
    		$scope.step2 = false;
    		$scope.step3 = false;
    		$scope.step4 = false;
    		$scope.step5 = false;

    		if(ele === '1'){
    			$scope.step1 = true;
    		}else if(ele === '2'){
    			$scope.step2 = true;
    		}else if(ele === '3'){
    			$scope.step3 = true;
    		}else if(ele === '4'){
    			$scope.step4 = true;
    		}else if(ele === '5'){
    			$scope.step5 = true;
    		}
    	};
    	

        $scope.active = function(ele) {
            $scope.step1 = '';
            $scope.step2 = '';
            $scope.step3 = '';
            $scope.step4 = '';
            $scope.step5 = '';
            $scope.ui = '';
            if (ele === '1') {
                $scope.step1 = 'active';
            } else if (ele === '2') {
                $scope.step2 = 'active';
            } else if (ele === '3') {
                $scope.step3 = 'active';
            } else if (ele === '4') {
                $scope.step4 = 'active';
            } else if (ele === '5') {
                $scope.step5 = 'active';
            } else if (ele === 'ui') {
                $scope.ui = 'active';
            }
        };

        $scope.load = function(evt) {
        	
            if (evt === '1') {
                $http.get('data/example1.json').
                then(function(response) {
                    // this callback will be called asynchronously
                    // when the response is available
                    //console.log(response.data);
                    $scope.title = response.data.title;
                    $scope.question = response.data.question;
                    $scope.inputinfoone = response.data.inputinfo.one;
                    $scope.inputinfotwo = response.data.inputinfo.two;
                    $scope.inputinfothree = response.data.inputinfo.three;

                    $scope.variableone = response.data.variables.one;
                    $scope.variabletwo = response.data.variables.two;
                    $scope.inputbeforeone = response.data.inputbefore.one;
                    $scope.inputbeforetwo = response.data.inputbefore.two;
                    $scope.inputadjustone = response.data.adjustbefore.one;
                    $scope.inputadjusttwo = response.data.adjustbefore.two;
                    $scope.changeone = response.data.change.one;
                    $scope.changetwo = response.data.change.two;
                    $scope.afterone = response.data.after.one;
                    $scope.aftertwo = response.data.after.two;
                    $scope.final = response.data.final;
                    $scope.calculate = function(z, x, c) {
                        $scope.add = z + c;
                        $scope.substract = x - z;
                        $scope.factor = c / $scope.substract;
                    };

                }, function(response) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
                    console.log(response);
                });
            } else if (evt === '2') {
                $http.get('data/example2.json').then(function(response) {
                    $scope.title = response.data.title;
                    $scope.question = response.data.question;
                    $scope.inputinfoone = response.data.inputinfo.one;
                    $scope.inputinfotwo = response.data.inputinfo.two;
                    $scope.inputinfothree = response.data.inputinfo.three;

                    $scope.variableone = response.data.variables.one;
                    $scope.variabletwo = response.data.variables.two;
                    $scope.inputbeforeone = response.data.inputbefore.one;
                    $scope.inputbeforetwo = response.data.inputbefore.two;
                    $scope.inputadjustone = response.data.adjustbefore.one;
                    $scope.inputadjusttwo = response.data.adjustbefore.two;
                    $scope.changeone = response.data.change.one;
                    $scope.changetwo = response.data.change.two;
                    $scope.afterone = response.data.after.one;
                    $scope.aftertwo = response.data.after.two;
                    $scope.final = response.data.final;
                    $scope.calculate = function(z, x, c) {
                        $scope.add = z + c;
                        $scope.substract = x - z;
                        $scope.factor = c / $scope.substract;
                    };

                });
            }

        };


    })
    .controller('BookCtrl', function($scope, $routeParams) {
        $scope.name = "BookCtrl";
        $scope.params = $routeParams;

    })
    .controller('ChapterCtrl', function($scope, $routeParams) {
        $scope.name = "ChapterCtrl";
        $scope.params = $routeParams;
    });